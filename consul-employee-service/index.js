const express = require('express')
const bodyParser = require('body-parser')
require('dotenv').config()

const PORT = process.env.PORT

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

const employees = [
  {
    'id': 1,
    'name': 'John Doe',
    'company_id': 1
  },
  {
    'id': 2,
    'name': 'Dohn Cloe',
    'company_id': 1
  },
  {
    'id': 3,
    'name': 'Rommy Din',
    'company_id': 2
  },
  {
    'id': 4,
    'name': 'Violet',
    'company_id': 2
  },
  {
    'id': 5,
    'name': 'Boby',
    'company_id': 3
  }
]

app.get('/', (req, res, next) => {
	res.json({"message": "It works!"})
})

app.post('/msg', (req, res, next) => {
	res.json({"message": req.body.message})
})

app.get('/msg', (req, res, next) => {
	res.json({"message": "Hello World!"})
})

app.get('/employees', (req, res, next) => {
	res.json(employees)
})

app.get('/employees/:id', (req, res, next) => {
  res.json(searchById(parseInt(req.params.id)))
})

app.get('/employees/company/:company_id', (req, res, next) => {
  res.json(searchByCompanyId(parseInt(req.params.company_id)))
})

let searchById = (value) => {
  let result = employees.filter(obj => {
    return obj.id === value
  })

  return result[0]
}

let searchByCompanyId = (value) => {
  let result = employees.filter(obj => {
    return obj.company_id === value
  })

  return result
}

app.listen(PORT, () => {
	console.log(`app listen on port ${PORT}`)
})