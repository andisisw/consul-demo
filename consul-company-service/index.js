const express = require('express')
const bodyParser = require('body-parser')
const axios = require('axios')
require('dotenv').config()

const PORT = process.env.PORT
const EMPLOYEE_SERVICE_URL = process.env.EMPLOYEE_SERVICE_URL

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

const companies = [
  {
    'id': 1,
    'name': 'Company 1'
  },
  {
    'id': 2,
    'name': 'Company 2'
  },
  {
    'id': 3,
    'name': 'Company 3'
  }
]

app.get('/', (req, res, next) => {
	res.json({"message": "It works!"})
})

app.get('/companies', (req, res, next) => {
	res.json(companies)
})

app.get('/companies/:id', (req, res, next) => {
	res.json(searchById(parseInt(req.params.id)))
})

app.post('/companies/employee/msg', async (req, res, next) => {
  try {
    let employee = await axios.post(EMPLOYEE_SERVICE_URL + "/msg", {message: req.body.message})
    res.json(employee.data)
  } catch (err) {
    res.status(403);
    res.json({"message": "You don't have permission to access this server."})
    next(err)
  }
})

app.get('/companies/employee/msg', async (req, res, next) => {
  try {
    let employee = await axios.get(EMPLOYEE_SERVICE_URL + "/msg")
    res.json(employee.data)
  } catch (err) {
    res.status(403);
    res.json({"message": "You don't have permission to access this server."})
    next(err)
  }
})

app.get('/companies/:company_id/employees', async (req, res, next) => {
  try {
    let employees = await axios.get(EMPLOYEE_SERVICE_URL + "/employees/company/" + req.params.company_id)
    res.json(employees.data)
  } catch (err) {
    res.status(403);
    res.json({"message": "You don't have permission to access this server."})
    next(err)
  }
})

let searchById = (value) => {
  let result = companies.filter(obj => {
    return obj.id === value
  })

  return result[0]
}

app.listen(PORT, () => {
	console.log(`app listen on port ${PORT}`)
})