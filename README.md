# Consul Demo #

Simple API for load testing consul.io

## Service 1 - Company Service ##

**Build Docker Image**

Adjust `.env` before build image

```
$ cd consul-company-service
$ make build
```

**Create & Start Container**

```
$ make run-docker
```

## Service 2 - Employee Service ##

**Build Docker Image**

Adjust `.env` before build image

```
$ cd consul-employee-service
$ make build
```

**Create & Start Container**

```
$ make run-docker
```